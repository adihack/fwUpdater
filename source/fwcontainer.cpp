#include "fwContainer.h"
#include <qdatastream.h>

fwContainer::fwContainer(QString firmwarePath) : QObject()
{
    mFilename = firmwarePath;
    mIsInitializedOK = false;
}

void fwContainer::init()
{
    QFile fwFile(mFilename);
    if (fwFile.open(QFile::ReadOnly))
    {
        mFw = fwFile.readAll();
        mFwSize = fwFile.size();
        Q_EMIT message("Firmware opened sucessfully.");
        Q_EMIT message(QString("Firmware type: plain, size: %1").arg(mFwSize));
        mIsInitializedOK = true;
    }
    else
    {
        Q_EMIT message(QString("Unable to open firmware file:"));
        mIsInitializedOK = false;
    }
}

QString fwContainer::getFwName()
{
    return "";
}

fwContainer::fwType fwContainer::getFwType()
{
    return mFwType;
}

int fwContainer::getFwSize()
{
    return mFwSize;
}

QByteArray fwContainer::getFwWord(uint16_t addr)
{
    QByteArray ret;

    ret.append(mFw[addr]);
    ret.append(mFw[addr+1]);

    return ret;
}

QByteArray fwContainer::getFw16b(uint16_t addr)
{
    QByteArray ret;

    for (int i = 0; ((i < 16) && ((addr+i) < mFw.size())); i++)
    {
        ret.append(mFw[addr + i]);
    }

    return ret;
}

bool fwContainer::isFw16bEmpty(uint16_t addr)
{
    QByteArray tmp = getFw16b(addr);
    if (tmp == QByteArray(16, (char)0xff)) return true;
        else return false;
}

bool fwContainer::isInitialized()
{
    return mIsInitializedOK;
}
