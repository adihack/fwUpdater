#include "fwUpdater.h"
#include "ui_fwupdater.h"


#include <QtSerialPort/QtSerialPort>
#include <QFileDialog>
#include <QString>

bool fwUpdater::initCommunicationWorker()
{   
    /*if ((FwContainer == nullptr) || (!FwContainer->isInitialized()))
    {
        ui->textBrowser->append("Please select firmware file first!");
        return false;
    }*/

    DeviceNode = new deviceNode();
    if (ui->radioButtonID->isChecked())
    {
        DeviceNode->setID(ui->IDLineEdit->text().toUInt(nullptr, 16));
    }
    else
    {
        DeviceNode->setUID(convertUID(ui->UIDLineEdit->text()));
    }

    if (ui->serialPortComboBox->currentText().isEmpty())
    {
        ui->textBrowser->append("Please select node port first!");
        return false;
    }

    communicationWorker = new communicationWorkerSerialPort(FwContainer, DeviceNode);
    communicationWorker->setPortName(ui->serialPortComboBox->currentText());
    workerThread = new QThread();
    communicationWorker->moveToThread(workerThread);

    QObject::connect(communicationWorker, SIGNAL(message(QString)), ui->textBrowser, SLOT(append(QString)));
    QObject::connect(communicationWorker, SIGNAL(progress(int)), ui->progressBar, SLOT(setValue(int)));
    QObject::connect(this, SIGNAL(setCommunicationWorkerPortName(QString)),communicationWorker, SLOT(setPortName(QString)));
    QObject::connect(ui->buttonConnectBootloader, SIGNAL(clicked()), communicationWorker, SLOT(connectBootloader()));
    QObject::connect(ui->buttonReadDeviceStatus, SIGNAL(clicked()), communicationWorker, SLOT(readDeviceStatus()));
    QObject::connect(ui->buttonSaveConfig, SIGNAL(clicked()), communicationWorker, SLOT(saveConfig()));
    QObject::connect(ui->buttonRebootDevice, SIGNAL(clicked()), communicationWorker, SLOT(restartDevice()));
    QObject::connect(ui->buttonStartApplication, SIGNAL(clicked()), communicationWorker, SLOT(startApplication()));
    QObject::connect(ui->buttonUpdateFirmware, SIGNAL(clicked()), communicationWorker, SLOT(updateFirmware()));
    QObject::connect(this, SIGNAL(changeIDSignal(char)), communicationWorker, SLOT(setNewID(char)));
    QObject::connect(this, SIGNAL(addCardSignal(QByteArray)), communicationWorker, SLOT(addCard(QByteArray)));
    QObject::connect(ui->buttonClearCards, SIGNAL(clicked()), communicationWorker, SLOT(clearCards()));

    workerThread->start();

    return true;
}

fwUpdater::fwUpdater(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::fwUpdater)
{
    ui->setupUi(this);
    updateSerialPortComboBox();
    FwContainer = nullptr;
    DeviceNode = nullptr;
}

fwUpdater::~fwUpdater()
{
    delete ui;
}

void fwUpdater::updateSerialPortComboBox()
{
    ui->serialPortComboBox->clear();
    foreach (const QSerialPortInfo &Port, QSerialPortInfo::availablePorts())
    {
        ui->serialPortComboBox->addItem(Port.portName());
    }
}

void fwUpdater::on_pushButtonSerialPortsRefresh_pressed()
{
    updateSerialPortComboBox();
}

void fwUpdater::on_buttonPushOpenFw_pressed()
{
    mFirmwarePath = QFileDialog::getOpenFileName(this, "Open Firmware File", "", "Firmware (*.cfw *.bin)");
    ui->textBrowser->append(QString("Select firmware file: %1").arg(mFirmwarePath));

    FwContainer = new fwContainer(mFirmwarePath);
    QObject::connect(FwContainer, SIGNAL(message(QString)), ui->textBrowser, SLOT(append(QString)));
    FwContainer->init();
}

void fwUpdater::on_buttonAddCard_pressed()
{
    ui->textBrowser->append(QString("Add card"));
    QByteArray tmpsn = QByteArray::fromHex(ui->lineEdit->text().toLatin1());
    Q_EMIT addCardSignal(tmpsn);
}

void fwUpdater::on_buttonSetNewID_pressed()
{
    ui->textBrowser->append(QString("Change ID"));
    char tmpid = QByteArray::fromHex(ui->NewIDLineEdit->text().toLatin1())[0];
    Q_EMIT changeIDSignal(tmpid);
}

void fwUpdater::on_buttonInitCommunication_pressed()
{
    if (initCommunicationWorker())
    {
        ui->buttonCloseCummunication->setEnabled(true);
        ui->buttonConnectBootloader->setEnabled(true);
        ui->buttonReadDeviceStatus->setEnabled(true);
        ui->buttonUpdateFirmware->setEnabled(true);
        ui->buttonStartApplication->setEnabled(true);
        ui->buttonRebootDevice->setEnabled(true);
        ui->buttonInitCommunication->setEnabled(false);
        ui->buttonPushOpenFw->setEnabled(false);
        ui->radioButtonID->setEnabled(false);
        ui->radioButtonUID->setEnabled(false);
        ui->serialPortComboBox->setEnabled(false);
        ui->IDLineEdit->setEnabled(false);
        ui->UIDLineEdit->setEnabled(false);
    }
}

void fwUpdater::on_buttonCloseCummunication_pressed()
{
    workerThread->terminate();
    delete communicationWorker;

    ui->buttonCloseCummunication->setEnabled(false);
    ui->buttonConnectBootloader->setEnabled(false);
    ui->buttonReadDeviceStatus->setEnabled(false);
    ui->buttonUpdateFirmware->setEnabled(false);
    ui->buttonStartApplication->setEnabled(false);
    ui->buttonRebootDevice->setEnabled(false);
    ui->buttonInitCommunication->setEnabled(true);
    ui->buttonPushOpenFw->setEnabled(true);
    ui->radioButtonID->setEnabled(true);
    ui->radioButtonUID->setEnabled(true);
    ui->serialPortComboBox->setEnabled(true);
    ui->IDLineEdit->setEnabled(true);
    ui->UIDLineEdit->setEnabled(true);
}

void fwUpdater::on_serialPortComboBox_currentTextChanged()
{
    Q_EMIT setCommunicationWorkerPortName(ui->serialPortComboBox->currentText());
}

void fwUpdater::on_UIDLineEdit_textChanged()
{
    DeviceNode->setUID(convertUID(ui->UIDLineEdit->text()));
}

QByteArray fwUpdater::convertUID(QString textUID)
{
    QStringList textUID_splited = textUID.split(":");

    QByteArray ret;
    foreach(QString s, textUID_splited)
    {
        ret.append(s.toUInt(nullptr, 16));
    }

    return ret;
}
