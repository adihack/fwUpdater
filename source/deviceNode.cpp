#include "deviceNode.h"

deviceNode::identityMode deviceNode::getIdentityMode()
{
    return mIdentityMode;
}

deviceNode::deviceNode()
{

}

void deviceNode::setUID(uint8_t vByte1, uint8_t vByte2, uint8_t vByte3, uint8_t vByte4)
{
    mIdentityMode = UID;

    mUID.clear();
    mUID.append(vByte1);
    mUID.append(vByte2);
    mUID.append(vByte3);
    mUID.append(vByte4);
}

void deviceNode::setUID(QByteArray vUID)
{
    mIdentityMode = UID;

    mUID.clear();
    mUID = vUID;
}

QByteArray deviceNode::getUID()
{
    return mUID;
}

QByteArray deviceNode::getKey()
{
    return mKey;
}

void deviceNode::setID(uint8_t vID)
{
    mIdentityMode = ID;
    mID = vID;
}

uint8_t deviceNode::getID()
{
    return mID;
}
