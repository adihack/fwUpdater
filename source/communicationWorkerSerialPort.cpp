#include "communicationWorkerSerialPort.h"
#include "fwUpdater.h"
#include <QThread>

communicationWorkerSerialPort::communicationWorkerSerialPort(fwContainer *vFwContainer, deviceNode *vDeviceNode) : communicationWorker(vFwContainer, vDeviceNode)
{

}

void communicationWorkerSerialPort::connectBootloader()
{
    Q_EMIT message("Connecting to bootloader..");

    modbusWrapper oModbusWrapper;
    QByteArray payload;
    payload.append(0x50);
    oModbusWrapper.setPayload(payload);
    oModbusWrapper.setNode(*mDeviceNode);

    QByteArray modbusDataPacket = oModbusWrapper.getDataPacket();
    Q_EMIT message(modbusDataPacket.toHex());

    openSerialPort();
    mSerialPort.write(modbusDataPacket);
    while(mSerialPort.waitForBytesWritten(1000));

    QByteArray modbusResponse = mSerialPort.readAll();
    Q_EMIT message(modbusResponse.toHex());

}

void communicationWorkerSerialPort::readDeviceStatus()
{
    Q_EMIT message("Reading device status..");
    Q_EMIT message("Opening serial port: " + mSerialPortName);
    openSerialPort();

    modbusWrapper oModbusWrapper;
    QByteArray payload;
    payload.append((uint8_t)0x31);
    payload.append((char)0x00);
    oModbusWrapper.setPayload(payload);
    oModbusWrapper.setNode(*mDeviceNode);

    QByteArray modbusDataPacket = oModbusWrapper.getDataPacket();
    Q_EMIT message(modbusDataPacket.toHex());

    mSerialPort.write(modbusDataPacket);
    while(mSerialPort.waitForBytesWritten(1000));

    QByteArray modbusResponse = mSerialPort.readAll();
    Q_EMIT message(modbusResponse.toHex());
}

void communicationWorkerSerialPort::updateFirmware()
{
    Q_EMIT message("Starting firmware update..");

    if (mFwContainer != nullptr)
    {
        Q_EMIT message(QString("Firmware size: %1").arg(mFwContainer->getFwSize()));
        Q_EMIT message("Opening serial port: " + mSerialPortName);
    }
    else
    {
        Q_EMIT message("Firmware load problem!");
        return;
    }

    openSerialPort();

    Q_EMIT message("Erasing flash..");
    Q_EMIT progress(0);

    for (int i = 0; i < APP_SIZE; i+= BLOCK_SIZE)
    {
        eraseBlock(APP_BASE_ADDRESS+i);
        QThread::msleep(50);
        Q_EMIT progress(10*(i+BLOCK_SIZE)/APP_SIZE);
    }

    Q_EMIT message("Erase complete!");
    Q_EMIT message("Writing data..");

    QByteArray a;

    /*
    for (int i = 0; i < APP_SIZE; i+= 2)
    {
        if (!((mFwContainer->getFwWord(i)[0] == (char)0xff) && (mFwContainer->getFwWord(i)[1] == (char)0xff)))
        {
            a.clear();
            a.append(mFwContainer->getFwWord(i));
            writeWord(APP_BASE_ADDRESS+i, a);
        }
        Q_EMIT progress(10+((i+2)*90/APP_SIZE));
    } */

    for (int i = 0; i < APP_SIZE; i+= 16)
    {
        if (!mFwContainer->isFw16bEmpty(i))
        {
            a.clear();
            a.append(mFwContainer->getFw16b(i));
            write16b(i, a);
        }
        Q_EMIT progress(10+((i+16)*90/APP_SIZE));
    }

    Q_EMIT message("Write complete!");

}

void communicationWorkerSerialPort::startApplication()
{
    Q_EMIT message("Starting bootloader..");
    Q_EMIT message("Opening serial port: " + mSerialPortName);
    openSerialPort();

    modbusWrapper oModbusWrapper;
    QByteArray payload;
    payload.append((uint8_t)0x34);
    payload.append((char)0x01);
    oModbusWrapper.setPayload(payload);
    oModbusWrapper.setNode(*mDeviceNode);

    QByteArray modbusDataPacket = oModbusWrapper.getDataPacket();
    Q_EMIT message(modbusDataPacket.toHex());

    mSerialPort.write(modbusDataPacket);
    while(mSerialPort.waitForBytesWritten(1000));

    QByteArray modbusResponse = mSerialPort.readAll();
    Q_EMIT message(modbusResponse.toHex());
}

void communicationWorkerSerialPort::setNewID(char id)
{
    Q_EMIT message("Changing ID..");
    Q_EMIT message("Opening serial port: " + mSerialPortName);
    openSerialPort();

    modbusWrapper oModbusWrapper;
    QByteArray payload;
    payload.append((uint8_t)0x36);
    payload.append((char)id);
    oModbusWrapper.setPayload(payload);
    oModbusWrapper.setNode(*mDeviceNode);

    QByteArray modbusDataPacket = oModbusWrapper.getDataPacket();
    Q_EMIT message(modbusDataPacket.toHex());

    mSerialPort.write(modbusDataPacket);
    while(mSerialPort.waitForBytesWritten(1000));

    QByteArray modbusResponse = mSerialPort.readAll();
    Q_EMIT message(modbusResponse.toHex());
}

void communicationWorkerSerialPort::addCard(QByteArray sn)
{
    Q_EMIT message("Adding card..");
    Q_EMIT message("Opening serial port: " + mSerialPortName);
    openSerialPort();

    modbusWrapper oModbusWrapper;
    QByteArray payload;
    payload.append((uint8_t)0x3A);
    payload.append((char)0x01);
    payload.append((char)0x01);
    payload.append((char)sn.at(0));
    payload.append((char)sn.at(1));
    payload.append((char)sn.at(2));
    payload.append((char)sn.at(3));
    oModbusWrapper.setPayload(payload);
    oModbusWrapper.setNode(*mDeviceNode);

    QByteArray modbusDataPacket = oModbusWrapper.getDataPacket();
    Q_EMIT message(modbusDataPacket.toHex());

    mSerialPort.write(modbusDataPacket);
    while(mSerialPort.waitForBytesWritten(1000));

    QByteArray modbusResponse = mSerialPort.readAll();
    Q_EMIT message(modbusResponse.toHex());
}

void communicationWorkerSerialPort::clearCards()
{
    Q_EMIT message("Clearing user cards list..");
    Q_EMIT message("Opening serial port: " + mSerialPortName);
    openSerialPort();

    modbusWrapper oModbusWrapper;
    QByteArray payload;
    payload.append((uint8_t)0x3B);
    payload.append((char)0x01);
    oModbusWrapper.setPayload(payload);
    oModbusWrapper.setNode(*mDeviceNode);

    QByteArray modbusDataPacket = oModbusWrapper.getDataPacket();
    Q_EMIT message(modbusDataPacket.toHex());

    mSerialPort.write(modbusDataPacket);
    while(mSerialPort.waitForBytesWritten(1000));

    QByteArray modbusResponse = mSerialPort.readAll();
    Q_EMIT message(modbusResponse.toHex());
}

void communicationWorkerSerialPort::saveConfig()
{
    Q_EMIT message("Saving configuration..");
    Q_EMIT message("Opening serial port: " + mSerialPortName);
    openSerialPort();

    modbusWrapper oModbusWrapper;
    QByteArray payload;
    payload.append((uint8_t)0x3D);
    payload.append((char)0x00);
    oModbusWrapper.setPayload(payload);
    oModbusWrapper.setNode(*mDeviceNode);

    QByteArray modbusDataPacket = oModbusWrapper.getDataPacket();
    Q_EMIT message(modbusDataPacket.toHex());

    mSerialPort.write(modbusDataPacket);
    while(mSerialPort.waitForBytesWritten(1000));

    QByteArray modbusResponse = mSerialPort.readAll();
    Q_EMIT message(modbusResponse.toHex());
}

void communicationWorkerSerialPort::restartDevice()
{
    Q_EMIT message("Restarting device..");
    Q_EMIT message("Opening serial port: " + mSerialPortName);

    Q_EMIT message("Connecting to bootloader..");

    modbusWrapper oModbusWrapper;
    QByteArray payload;
    payload.append(0x08);
    oModbusWrapper.setPayload(payload);
    oModbusWrapper.setNode(*mDeviceNode);

    QByteArray modbusDataPacket = oModbusWrapper.getDataPacket();
    Q_EMIT message(modbusDataPacket.toHex());

    openSerialPort();
    mSerialPort.write(modbusDataPacket);
    while(mSerialPort.waitForBytesWritten(1000));

    QByteArray modbusResponse = mSerialPort.readAll();
    Q_EMIT message(modbusResponse.toHex());
}

void communicationWorkerSerialPort::setPortName(QString vPortName)
{
    mSerialPortName = vPortName;
    Q_EMIT message("Serial port changed to: " + vPortName);
}

void communicationWorkerSerialPort::openSerialPort()
{
    mSerialPort.setPortName(mSerialPortName);
    mSerialPort.setBaudRate(QSerialPort::Baud38400);
    mSerialPort.open(QSerialPort::ReadWrite);
    if (mSerialPort.isOpen()) Q_EMIT message("Serial port successfully opened.");
        else Q_EMIT message("Unable to open serial port!");
}

communicationWorkerSerialPort::~communicationWorkerSerialPort()
{
    if (mSerialPort.isOpen()) mSerialPort.close();
}

int communicationWorkerSerialPort::eraseBlock(uint16_t vAddress)
{
    modbusWrapper oModbusWrapper;
    oModbusWrapper.setNode(*mDeviceNode);

    QByteArray payload;
    payload.append((char)CMD_ERASE_BLOCK);
    payload.append((char)(FLASH_BASE_ADDRESS >> 24));
    payload.append((char)(FLASH_BASE_ADDRESS >> 16));
    payload.append((char)(vAddress >> 8));
    payload.append((char)(vAddress));
    oModbusWrapper.setPayload(payload);

    QByteArray modbusDataPacket = oModbusWrapper.getDataPacket();
    //Q_EMIT message(modbusDataPacket.toHex());

    mSerialPort.write(modbusDataPacket);
    while(mSerialPort.waitForBytesWritten(1000));
    QThread::msleep(500);

    QByteArray modbusResponse = mSerialPort.readAll();
    //Q_EMIT message(modbusResponse.toHex());

    for(int i = 0; i < 50; i++)
    {
        mSerialPort.waitForReadyRead(10);
        modbusResponse.append(mSerialPort.readAll());
        if (modbusResponse.size() >= 4) break;
    }

    if (modbusResponse.size() == 0)
    {
        Q_EMIT message("Device node timeout!");
        return 1;
    }
    else
    {
        if (verifyResponse(modbusResponse) == 0x01) return 0;
        else
        {
            Q_EMIT message("Wrong answer from device!");
            return 3;
        }
    }

    return 0;
}

int communicationWorkerSerialPort::writeWord(uint16_t vAddress, QByteArray vData)
{
    modbusWrapper oModbusWrapper;
    oModbusWrapper.setNode(*mDeviceNode);

    QByteArray payload;
    payload.append((char)CMD_WRITE_WORD);
    payload.append((char)(FLASH_BASE_ADDRESS >> 24));
    payload.append((char)(FLASH_BASE_ADDRESS >> 16));
    payload.append((char)(vAddress >> 8));
    payload.append((char)(vAddress));
    payload.append((char)(vData[1]));
    payload.append((char)(vData[0]));
    oModbusWrapper.setPayload(payload);

    QByteArray modbusDataPacket = oModbusWrapper.getDataPacket();
    //Q_EMIT message(modbusDataPacket.toHex());

    mSerialPort.write(modbusDataPacket);
    if (!mSerialPort.waitForBytesWritten(500))
    {
        Q_EMIT message("Error occurred during write to serial port!");
        return 2;
    }
    QThread::msleep(100);

    QByteArray modbusResponse;

    for(int i = 0; i < 50; i++)
    {
        mSerialPort.waitForReadyRead(10);
        modbusResponse.append(mSerialPort.readAll());
        if (modbusResponse.size() >= 4) break;
    }

    if (modbusResponse.size() == 0)
    {
        Q_EMIT message("Device node timeout!");
        return 1;
    }
    else
    {
        if (verifyResponse(modbusResponse) == 0x01) return 0;
        else
        {
            Q_EMIT message("Wrong answer from device!");
            return 3;
        }
    }

    return 0;
}

int communicationWorkerSerialPort::write16b(uint16_t vAddress, QByteArray vData)
{
    modbusWrapper oModbusWrapper;
    oModbusWrapper.setNode(*mDeviceNode);

    QByteArray payload;
    payload.append((char)CMD_WRITE_16B);
    payload.append((char)(vAddress >> 8));
    payload.append((char)(vAddress));
    payload.append(vData);
    oModbusWrapper.setPayload(payload);

    QByteArray modbusDataPacket = oModbusWrapper.getDataPacket();
    //Q_EMIT message(modbusDataPacket.toHex());

    mSerialPort.write(modbusDataPacket);
    if (!mSerialPort.waitForBytesWritten(500))
    {
        Q_EMIT message("Error occurred during write to serial port!");
        return 2;
    }

    QByteArray modbusResponse;

    for(int i = 0; i < 50; i++)
    {
        mSerialPort.waitForReadyRead(10);
        modbusResponse.append(mSerialPort.readAll());
        if (modbusResponse.size() >= 4) break;
    }

    if (modbusResponse.size() == 0)
    {
        Q_EMIT message("Device node timeout!");
        return 1;
    }
    else
    {
        if (verifyResponse(modbusResponse) == 0x01) return 0;
        else
        {
            Q_EMIT message("Wrong answer from device!");
            return 3;
        }
    }

    return 0;
}

int communicationWorkerSerialPort::verifyResponse(QByteArray vBuffer)
{
    QByteArray tmpData;

    for (int i = 0; i < (vBuffer.size()-2); i++)
    {
        tmpData.append(vBuffer[i]);
    }

    QByteArray tmpCrc = getModbusCRC(tmpData);
    if ((tmpCrc[0] != vBuffer[vBuffer.size()-1]) || (tmpCrc[1] != vBuffer[vBuffer.size()-2]))
    {
        QString s;
        s.append("Bad crc! Expected: ");
        s.append(tmpCrc.toHex());
        s.append(" got: ");
        Q_EMIT message(s);
        Q_EMIT message(tmpData.toHex());
        return 255;
    }
    else
    {
        Q_EMIT message("Node respond: OK");
        return (int)vBuffer[1];
    }
}

QByteArray communicationWorkerSerialPort::getModbusCRC(QByteArray vData)
{
    uint16_t crc = 0xFFFF;

    for (int pos = 0; pos < vData.length(); pos++)
    {
        crc ^= (uint16_t)((uint8_t)(vData[pos]));        // XOR byte into least sig. byte of crc

        for (int i = 8; i != 0; i--)
        {                                 // Loop over each bit
            if ((crc & 0x0001) != 0)
            {                             // If the LSB is set
                crc >>= 1;                // Shift right and XOR 0xA001
                crc ^= 0xA001;
            }
            else
            {                             // Else LSB is not set
                crc >>= 1;                // Just shift right
            }
        }
    }

    QByteArray crc16;
    crc16.append(crc & 0xff);
    crc16.append(crc >> 8);
    return crc16;
}

