#include "modbusWrapper.h"
#include <QtEndian>

modbusWrapper::modbusWrapper()
{

}

void modbusWrapper::setPayload(QByteArray vData)
{
    dataPayload = vData;
}

void modbusWrapper::setNode(deviceNode vNode)
{
    currentDeviceNode = vNode;
}

QByteArray modbusWrapper::getDataPacket()
{
    dataPacket.clear();

    if (currentDeviceNode.getIdentityMode() == deviceNode::ID)
    {
        dataPacket.append(currentDeviceNode.getID());
    }
    else // UID
    {
        dataPacket.append((char)0x00); // use UID
        dataPacket.append(currentDeviceNode.getUID());
    }

    dataPacket.append(dataPayload);
    dataPacket.append(getModbusCRC(dataPacket));
    return dataPacket;
}

QByteArray modbusWrapper::getModbusCRC(QByteArray vData)
{
    uint16_t crc = 0xFFFF;

    for (int pos = 0; pos < vData.length(); pos++)
    {
        crc ^= (uint16_t)((uint8_t)(vData[pos]));        // XOR byte into least sig. byte of crc

        for (int i = 8; i != 0; i--)
        {                                 // Loop over each bit
            if ((crc & 0x0001) != 0)
            {                             // If the LSB is set
                crc >>= 1;                // Shift right and XOR 0xA001
                crc ^= 0xA001;
            }
            else
            {                             // Else LSB is not set
                crc >>= 1;                // Just shift right
            }
        }
    }

    QByteArray crc16;
    crc16.append(crc & 0xff);
    crc16.append(crc >> 8);
    return crc16;
}

