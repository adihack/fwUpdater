#-------------------------------------------------
#
# Project created by QtCreator 2017-03-17T22:43:26
#
#-------------------------------------------------

QT += core gui
QT += serialport
QT += widgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = fwUpdater
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

INCLUDEPATH += $$PWD/include

SOURCES += \
    source/communicationWorker.cpp \
    source/communicationWorkerSerialPort.cpp \
    source/deviceNode.cpp \
    source/fwContainer.cpp \
    source/fwUpdater.cpp \
    source/main.cpp \
    source/modbusWrapper.cpp

HEADERS  += \
    include/communicationWorker.h \
    include/communicationWorkerSerialPort.h \
    include/deviceNode.h \
    include/fwContainer.h \
    include/fwUpdater.h \
    include/modbusWrapper.h

FORMS    += \
    resources/fwUpdater.ui

RESOURCES += \
    resources/fwUpdater.qrc

DISTFILES += \
    resources/images/open-file-icon.png \
	resources/images/refresh-256x256.png