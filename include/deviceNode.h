#ifndef DEVICENODE_H
#define DEVICENODE_H

#include "QByteArray"

class deviceNode
{
public:
    enum identityMode {
        ID,
        UID
    };

    identityMode getIdentityMode();

    deviceNode();
    void setUID(uint8_t vByte1, uint8_t vByte2, uint8_t vByte3, uint8_t vByte4);
    void setUID(QByteArray vUID);
    void setID(uint8_t vID);
    QByteArray getUID();
    QByteArray getKey();
    uint8_t getID();
private:
    QByteArray mUID;
    QByteArray mKey;
    uint8_t mID;
    QByteArray sessionCurrentSeed;
    identityMode mIdentityMode;
};

#endif // DEVICENODE_H
