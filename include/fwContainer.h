#ifndef FWCONTAINER_H
#define FWCONTAINER_H

#include "QFile"
#include "QByteArray"

class fwContainer : public QObject
{
    Q_OBJECT

public:
    fwContainer(QString firmwarePath);
    void init();
    enum fwType
       {
          fwTypePlain,
          fwTypeEncrypted
       };
    QString getFwName();
    fwType getFwType();
    int getFwSize();
    QByteArray getFwWord(uint16_t addr);
    QByteArray getFw16b(uint16_t addr);
    bool isFw16bEmpty(uint16_t addr);
    bool isInitialized();
    
private:
    QFile mFwFile;
    QString mFilename;
    int mFwSize;
    fwType mFwType;
    QByteArray mFw;
    bool mIsInitializedOK;


signals:
    void message(QString msg);

};

#endif // FWCONTAINER_H
