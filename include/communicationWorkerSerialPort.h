#ifndef SERIALPORT_WORKER_H
#define SERIALPORT_WORKER_H

#include "communicationWorker.h"
#include "modbusWrapper.h"
#include <QtSerialPort/QtSerialPort>

class communicationWorkerSerialPort : public communicationWorker
{
    Q_OBJECT

public:
    communicationWorkerSerialPort(fwContainer *vFwContainer, deviceNode *vDeviceNode);
    ~communicationWorkerSerialPort();

public slots:
    virtual void connectBootloader();
    virtual void readDeviceStatus();
    virtual void updateFirmware();
    virtual void startApplication();
    virtual void setNewID(char id);
    virtual void addCard(QByteArray sn);
    virtual void clearCards();
    virtual void saveConfig();
    virtual void restartDevice();
    virtual void setPortName(QString vPortName);

private:
    QString mSerialPortName;
    QSerialPort mSerialPort;
    void openSerialPort();
    virtual int eraseBlock(uint16_t vAddress);
    virtual int writeWord(uint16_t vAddress, QByteArray vData);
    virtual int write16b(uint16_t vAddress, QByteArray vData);
    virtual int verifyResponse(QByteArray vBuffer);
    QByteArray getModbusCRC(QByteArray vData);

};

#endif // SERIALPORT_WORKER_H
