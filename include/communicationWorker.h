#ifndef COMMUNICATIONWORKER_H
#define COMMUNICATIONWORKER_H

#include <QObject>
#include "fwContainer.h"
#include "deviceNode.h"

#define CMD_ERASE_BLOCK         0x51
#define CMD_WRITE_WORD          0x52
#define CMD_WRITE_16B           0x53
#define FLASH_BASE_ADDRESS      0x08000000
#define BOOT_BASE_ADDRESS       0x0000
#define APP_BASE_ADDRESS        0x3000
#define BLOCK_SIZE              0x400
#define BOOT_SIZE               0x3000
#define APP_SIZE                0x4000

class communicationWorker : public QObject
{
    Q_OBJECT

public:
    communicationWorker(fwContainer *vFwContainer, deviceNode *vDeviceNode);
    //void setFwContainer(fwContainer &vFwContainer);
    //void setDeviceNode(deviceNode &vDeviceNode);

signals:
    void message(QString msg);
    void progress(int val);

public slots:
    virtual void connectBootloader() = 0;
    virtual void readDeviceStatus() = 0;
    virtual void updateFirmware() = 0;
    virtual void startApplication() = 0;
    virtual void setNewID(char id) = 0;
    virtual void addCard(QByteArray sn) = 0;
    virtual void clearCards() = 0;
    virtual void saveConfig() = 0;
    virtual void restartDevice() = 0;
    virtual void setPortName(QString vPortName) = 0;

protected:
    fwContainer *mFwContainer;
    deviceNode *mDeviceNode;
    virtual int eraseBlock(uint16_t vAddress) = 0;
    virtual int writeWord(uint16_t vAddress, QByteArray vData) = 0;
    virtual int write16b(uint16_t vAddress, QByteArray vData) = 0;
    virtual int verifyResponse(QByteArray vBuffer) = 0;

};

#endif // COMMUNICATIONWORKER_H
