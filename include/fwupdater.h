#ifndef FWUPDATER_H
#define FWUPDATER_H

#include <QMainWindow>
#include <QString>
#include "communicationWorkerSerialPort.h"

namespace Ui {
class fwUpdater;
}

class fwUpdater : public QMainWindow
{
    Q_OBJECT

signals:
    void setCommunicationWorkerPortName(QString vPortName);
    void addCardSignal(QByteArray sn);
    void changeIDSignal(char id);

public:
    explicit fwUpdater(QWidget *parent = 0);
    ~fwUpdater();

private slots:
    void on_pushButtonSerialPortsRefresh_pressed();
    void on_serialPortComboBox_currentTextChanged();
    void on_buttonPushOpenFw_pressed();
    void on_UIDLineEdit_textChanged();
    void on_buttonInitCommunication_pressed();
    void on_buttonCloseCummunication_pressed();
    void on_buttonAddCard_pressed();
    void on_buttonSetNewID_pressed();

private:
    Ui::fwUpdater *ui;
    void updateSerialPortComboBox();
    bool initCommunicationWorker();
    QThread *workerThread;
    communicationWorker *communicationWorker;
    QByteArray convertUID(QString textUID);
    fwContainer *FwContainer;
    deviceNode *DeviceNode;
    QString mFirmwarePath;
};

#endif // FWUPDATER_H
