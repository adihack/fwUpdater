#ifndef MODBUSWRAPPER_H
#define MODBUSWRAPPER_H

#include "QByteArray"
#include "deviceNode.h"

class modbusWrapper
{
public:
    modbusWrapper();
    void setPayload(QByteArray vData);
    void setNode(deviceNode vNode);
    QByteArray getDataPacket();

private:
    QByteArray dataPacket;
    QByteArray dataPayload;
    deviceNode currentDeviceNode;
    QByteArray getModbusCRC(QByteArray vData);

};

#endif // MODBUSWRAPPER_H
